abstract class PollOptions {
  static const String IsLivePoll = 'm.disclosed';
  static const String IsNotLivePoll = 'm.undisclosed';
  static const String WithFreeText = 'm.with_freetext';
  static const String WithoutFreeText = 'm.no_freetext';
  static const String WithNames = 'm.with_names';
  static const String WithoutNames = 'm.no_names';
}
