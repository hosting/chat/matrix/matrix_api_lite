import 'matrix_event.dart';

class GetRelationsResponse {
  GetRelationsResponse({
    required this.chunk,
  });

  GetRelationsResponse.fromJson(Map<String, dynamic> json)
      : chunk = (json['chunk'] as List)
            .map((v) => MatrixEvent.fromJson(v as Map<String, dynamic>))
            .toList();
  Map<String, dynamic> toJson() {
    return {
      'chunk': chunk.map((v) => v.toJson()).toList(),
    };
  }

  /// A list of room events. The order depends on the `dir` parameter.
  /// For `dir=b` events will be in reverse-chronological order,
  /// for `dir=f` in chronological order. (The exact definition of `chronological`
  /// is dependent on the server implementation.)
  ///
  /// Note that an empty `chunk` does not *necessarily* imply that no more events
  /// are available. Clients should continue to paginate until no `end` property
  /// is returned.
  List<MatrixEvent> chunk;
}
